## Examples

```json
[
    {
        "@id": "cpu",
    },
    {
        "@id": "drive",
    }
]
```

## MVP

Components: emulators:$EMULATOR_NAME#$ARCHITECTURE-$TYPE

with:

$EMULATOR_NAME:

- qemu-system
- basiliskII
- ...

$ARCHITECTURE, for QEMU:

- x86_64
- ppc
- i386
- sparc

otherwise:

- default

$TYPE:

- disk (with path)
- cdrom (with path)
- floppy (with path)
- cart (with path)
- nic (with vdePath)



```json
{
    "machine": "emulators:qemu-system#x86_64",
    "hardwareComponents": [
    {
      "index": 1,
      "component": "emulators:qemu#x86_64-disk",
      "path": "/emucon/drives/drive1"
    },
    {
        "index": 2,
        "component": "emulators:qemu#x86_64-",
        "nativeConfig": [],
    }
    ], "frameworkComponents": [
    {
      "frameworkComponent": "framework:xpra",
      "path": "/emucon/xpra-iosock"
    }],
    "nativeConfig": ["-smp", "2"]
}
```


```json
{
    "dc:title": "Example Environment",
    "dc:created": "2021-08-16T10:00:00Z",
    "moreMetadata": "...",
    "emulatorImage": "...",
    "machine": "https://purl.org/emulation-archive/emulators/qemu#x86_64",
    "hardwareComponents": [
    {
      "index": 1,
      "component": "https://purl.org/emulation-archive/emulators/qemu#x86_64-harddisk",
      "@type": ["harddisk", "binding"],
      "source": "urn:uuid:b4080444-8f7c-433f-bbac-b41162fc4899",
      "path": "/emucon/drives/drive1"
    }], "frameworkComponents": [
    {
      "frameworkComponent": "https://purl.org/emulation-archive/framework#xpra",
      "@type": "socket",
      "path": "/emucon/xpra-iosock"
    }
    ]
}


{
    "dc:title": "QEMU Emulator 2.0.0",
    "emulator": "wd:Q624699",
    "supportedMachines": [
        {
            "@id": "https://purl.org/emulation-archive/emulators/qemu#x86_64", // x86_64, ppc, i386, sparc, default
            "@type": "eaas:machine",
            "dc:title": "IBM PC (x86_64/AMD64)",
            "nativeConfig": "",
            "defaultHardwareComponents": [
               {}, {}
            ],
            "supportedHardwareComponents": [
            {
                "@id": "#x86_64-disk", // -cdrom, -disk, -floppy, -cart
                "@type": "eaas:drive", // https://gitlab.com/emulation-as-a-service/eaas-server/blob/master/src/eaas/components/api/src/main/java/de/bwl/bwfla/emucomp/api/Drive.java#L309
                "media": "eaas:disk",
                "dc:title": "Default Harddrive",
                "wdt:P2283": "wd:Q230360"
            },
            { // native: -cpu, -kvm (virtualized)
                "@id": "#x86_64-"
            }
            // -memory (native)
            // -nic
            // -parallelport            ]
        },
           {
            "@id": "#ppc",
            "@type": "eaas:machine",
            "dc:title": "PowerPC (Macintosh, 32 bit)",
            "emulates": "http://www.wikidata.org/entity/Q209860",
             "nativeConfig": "",
            "supportedHardwareComponents": [
            {
                "@id": "#x86_64-harddisk",
                "@type": "eaas:diskdrive",
                "medium": "eaas:harddisk",
                "dc:title": "Default Harddrive",
                "wdt:P2283": "wd:Q230360"            }
            ]
        }
    ],
    "supportedFrameworkComponents": [
        {
                "@id": "#x86_64-xpra",
                "@type": "eaas:xpra",
                "dc:title": "Xpra 3.0",
        }
    ]
}
```

---

# Old

```
{
    "dc:title": "QEMU Emulator 2.0.0",
    "emulator": "wd:Q624699",
    "supportedMachines": [
        {
            "@id": "#x86_64", // x86_64, ppc, i386, sparc, default
            "@type": "eaas:machine",
            "dc:title": "IBM PC (x86_64/AMD64)",
            "nativeConfig": "",
            "defaultHardwareComponents": [
               {}, {}
            ],
            "supportedHardwareComponents": [
            {
                "@id": "#x86_64-disk", // -cdrom, -disk, -floppy, -cart
                "@type": "eaas:drive", // https://gitlab.com/emulation-as-a-service/eaas-server/blob/master/src/eaas/components/api/src/main/java/de/bwl/bwfla/emucomp/api/Drive.java#L309
                "media": "eaas:disk", 
                "dc:title": "Default Harddrive",
                "wdt:P2283": "wd:Q230360"
            },
            { // native: -cpu, -kvm (virtualized)
                "@id": "#x86_64-"
            }
            // -memory (native)
            // -nic
            // -parallelport
            
            ]
        },
           {
            "@id": "#ppc",
            "@type": "eaas:machine",
            "dc:title": "PowerPC (Macintosh, 32 bit)",
            "emulates": "http://www.wikidata.org/entity/Q209860",
             "nativeConfig": "",
            "supportedHardwareComponents": [
            {
                "@id": "#x86_64-harddisk",
                "@type": "eaas:diskdrive",
                "medium": "eaas:harddisk",
                "dc:title": "Default Harddrive",
                "wdt:P2283": "wd:Q230360"

            }
            ]
        }
    ],
    "supportedFrameworkComponents": [
        {
                "@id": "#x86_64-xpra",
                "@type": "eaas:xpra",
                "dc:title": "Xpra 3.0",
        }
    ]
}

{
    "dc:title": "Example Environment",
    "dc:created": "2021-08-16T10:00:00Z"
    "moreMetadata": "...",
    "machine": "https://purl.org/emulation-archive/4c83e2f1-dd00-471c-9a78-403b62e3123f/#x86_64"
    "hardwareComponents": [
    {
      "component": "https://purl.org/emulation-archive/4c83e2f1-dd00-471c-9a78-403b62e3123f/#x86_64-harddisk",
      "@type": "http://www.wikidata.org/entity/Q7564654",
      "index": 1,
      "binding": {
          "@type": "binding",
          "source": "urn:uuid:b4080444-8f7c-433f-bbac-b41162fc4899",
          "path": "/emucon/drives/drive1"
      }
    }], "frameworkComponents": [{
    {
      "frameworkComponent": "https://purl.org/emulation-archive/4c83e2f1-dd00-471c-9a78-403b62e3123f/#x86_64-xpra",
      "socket": {
          "@type": "socket",
          "path": "/emucon/xpra-iosock"
      }
    },
    ]
}

{
    "@type": "emulatorImage",
    {
        "@id": "#soudblaster16",
        "emualtes": "wd:Q7564654",
        "rdfs:subclassOf": ""
    }
}

{
    "@type": "emulationEnvironment",
    "title": "Environment 1",
    "emulationImage": "https://purl.org/emulation-archive/..."
    "hardwareComponents": [
        {
            "componentType": "https://purl.org/emulation-archive/...#soundblaster16",
        }
    ]

}
```

```json
{
    "title": "QEMU Emulator",
    "canEmulate": [
        {
            "components": [
            {
                "@type": "machine",
                "@id": "#pc",
                "emulates": "http://www.wikidata.org/entity/Q751046"
            },
            {
                "@type": "cpu",
                "@id": "#qemu-x86",
                "emulates": "http://www.wikidata.org/entity/Q221257",
                "architecture": "http://www.wikidata.org/entity/Q262238"
            },
            {
                "@type": "drive",
                "@id": "#ide",
                "interface": "https://www.wikidata.org/entity/Q230360"
            }
            ]
        },
        {
            "components": [
            {
                "@type": "machine",
                "@id": "#q800",
                "emulates": "http://www.wikidata.org/entity/Q4413173"
            },
            {
                "@type": "cpu",
                "@id": "#qemu-x86",
                "emulates": "http://www.wikidata.org/entity/Q667808",
                "architecture": "http://www.wikidata.org/entity/Q937498"
            },
            ]
        },
        {
            "components": [
            {
                "@type": "machine",
                "@id": "#next-cube",
                "emulates": "http://www.wikidata.org/entity/Q831367"
            },
            {
                "@type": "cpu",
                "@id": "#qemu-x86",
                "emulates": "http://www.wikidata.org/entity/Q667808",
                "architecture": "http://www.wikidata.org/entity/Q937498"
            },
            ]
        }
    ]

}
```

```json
{
    "title": "Example Environment",
    "created": "2021-08-16T10:00:00Z"
    "moreMetadata": "",
    "components": [
    {
      "hwType": "#ide",
      "hwOrder": "1",
      "binding": {
          "@type": "binding",
          "backing": "urn:uuid:b4080444-8f7c-433f-bbac-b41162fc4899",
          "path": "/emucon/drives/drive1"
      }
    },
    
    ]

}
```


## Problem

Emulators (or rather emulator containers, as there can be different implementations of the same emulator in EaaS) and (emulation) environments should be described using JSON-LD to make them

1. buildable by other people separately from EaaS,
2. accessible/reusable in other contexts,
3. enhanceable with third-party metadata/knowledge from sources like Wikidata/DBpedia/...

While 1. could be achieved by merely describing emulators and environments using JSON, a connection to the semantic world would have to be constructed separately for 3. anyway. Thus, having a single document serving both purposes as single source of truth seems much preferable. Furthermore, providing the data as JSON-LD instead of JSON helps with 2. by eliminating much work for third-party usage of the data.

Describing emulators using JSON-LD comes with challenges, as:

1. (complete) knowledge about the emulators is probably not available at the time of integrating them
   - new knowledge might be become available at a later time; you might only know that Windows 11 does not support QEMU 2.x's default drive type after Windows 11 is released
   - this can also include knowledge as simple as "which drive type does this emulator use by default?"; QEMU 2.x might provide disk drives in ATA/IDE mode by default, whereas QEMU 10.x might provide disk drives in AHCI mode (or even PCIe/NVMe drives) by default
2. the process of integrating new emulators should be as lightweight as possible
   - it puts too much burden on the person integrating the emulator to research all of this knowledge while integrating the emulator
   - some of the knowledge might only be generated experimentally using EaaS after the emulator has already been integrated, creating a chicken and egg problem
   - for some platforms, there might be only one emulator at the beginning, so that knowledge about their interoperability and comparability/compatibility, e.g., of drive types cannot yet be drawn
   - some emulators might not prove useful in practice after their integration, so as little effort as possible should be wasted to create elaborate metadata before it is needed
3. existing emulation environments should not have to be changed after their creation
   - they might have been tested only using a specific emulator/emulator configuration and should continue to use that exact configuration unless the user requests an update to a new emulator/emulator version 
   - they could be spread across many EaaS installations with different sets/versions of metadata, making it hard to compute needed environment updates when metadata gets updated/synchronized

## Solution

Keeping existing environment metadata stable while adding additional knowledge makes it necessary to describe the emulation environments using a two-step approach: The only thing certain at creation time of emulation environments is the exact chosen configuration of the emulator container in its chosen version. While the emulator container might not know what effect options passed to the emulator have (will `-hda` create an ATA or AHCI drive, will its storage controller be attached via PCI?), it can assign a (unique) identifier to every device/configuration type it supports and for which it passes different command-line arguments to the emulator. Linking these identifiers to, e.g., real-world devices can then be done as a second step, possibly at a later time, without modifying existing environments. In the same way, wrong assumptions about emulated devices (does QEMU really emulate an 80486 CPU by default?) can be corrected retroactively.

This not only allows to describe existing hardware types but, at least to some degree, allows to introduce new hardware categories in a single emulator first. For example, you might add different sound cards as generic hardware to a single emulator first by assigning a local identifier to each of them. When sound cards become a more generically useful hardware type, you can later make these identifiers instances (rdf:type, rdfs:subClassOf) of the abstract sound card by only changing the emulator's metadata (but not changing the single emulation environment's metadata at all!)

## Alternatives

It might be tempting to describe hardware emulated by the emulator directly using a Wikidata URL. For example, you could use "http://www.wikidata.org/entity/Q165233" to describe a network card and the emulator would choose a default card for the respective platform. While this might hold true for cases like disk drives (which the platform/the platform's firmware has to be able to access to even be able to boot at all, so there probably is a "default" hardware type choice for the platform), for slightly more complex hardware, like network cards, the assumption already breaks. While the Linux's kernels default x86 32-bit configuration has [support for NE2000-compatible network cards](https://github.com/torvalds/linux/blob/9e9fb7655ed585da8f468e29221f0ba194a5f613/arch/x86/configs/i386_defconfig#L154) (a popular hardware from the early x86 era), its default x86(-64) 64-bit configuration only has [support for E1000-compatible network cards](https://github.com/torvalds/linux/blob/9e9fb7655ed585da8f468e29221f0ba194a5f613/arch/x86/configs/x86_64_defconfig#L145) (a popular hardware from the x86-64 era). Thus, a default hardware type for a platform very often does not exist in the general case. Things get even harder if you take into account that the emulator might emulate an NE2000-compatible card attached to the ISA bus or the PCI bus by default or if you consider other hardware like sound cards, where a plethora of different (incompatible) popular choices existed in the early x86 era.

If you, thus, were to describe hardware directly using the a generic URL (e.g., <http://www.wikidata.org/entity/Q165233> for a network card) or directly using a Wikidata URL of the specific hardware you think the emulator emulates (e.g., <http://www.wikidata.org/entity/Q502465> for an NE2000-compatible card), you would not be able to later correct any mistakes (is QEMU 10.x really still using an NE2000-compatible card by default?, does it use ISA or PCI?) or describe new knowledge about the exact emulated hardware ("the NE2000-compatible card emulated by this specific QEMU 2.0.1 emulator container is incompatible with Windows 95") later on. For example, you cannot simply claim that, e.g., "http://www.wikidata.org/entity/Q502465" is incompatible with Windows 95 as this would make all NE2000-compatible cards incompatible with Windows 95.

Additionally, you would have to research all of these mappings of emulator options to real-world hardware at time of emulator integration before you can even start it for the first time, making this a very tedious process and hindering experimentation with emulators. You wold also have to push any knowledge and reasoning about emulators and platforms to the respective emulator containers without a chance to gain and incorporate more knowledge at a later time (as the emulator containers will have to be kept static for exact compatibility with existing emulation environments). Only describing the emulator's options and (possibly at a later time) link more knowledge to them from the outside allows an outside component to, e.g., through inference from existing/third-party knowledge about operating systems, choose the emulator's NE2000-compatible card option for 32-bit and the emulator's E1000-compatible card option for 64-bit x86 operating systems, or choose a sound card option compatible to a user-selected game.
